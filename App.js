import React, { Component } from 'react';
import ProgressBar from 'react-native-progress/Bar'; 
import Icon from 'react-native-vector-icons/FontAwesome';
import Modal from "react-native-modal";

var { Dimensions } = require('react-native')

import {
  Platform,
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  Picker
} from 'react-native';

const instructions = Platform.select({
  ios: 'Press Cmd+R to reload,\n' +
    'Cmd+D or shake for dev menu',
  android: 'Double tap R on your keyboard to reload,\n' +
    'Shake or press menu button for dev menu',
});

export default class App extends Component {

  constructor(props) {
    super(props);

    this.state = {
      progress: 0,
      complete: false,
      PickerValue: ''
    };
  }

  state = {
    visibleModal: null,
  };

  
  _renderButton = (text, onPress) => (
    <TouchableOpacity onPress={onPress}>
      <View style={styles.button}>
        <Text>{text}</Text>
      </View>
    </TouchableOpacity>
  );

  _renderModalContent = () => (
    <View style={styles.modalContent}>
      <Text>Filter </Text>
      <Picker
        style={{width:'80%'}}
        selectedValue={this.state.PickerValue}
        onValueChange={(itemValue,itemIndex) => this.setState({PickerValue:itemValue})}
        >
        <Picker.Item label="Expat" value="Expat" />
        <Picker.Item label="UAE National" value="UAE"/>
      </Picker>
    </View>
  );

  clickme=()=>{
		var data = this.state.PickerValue;
		if (data == " ") {
			alert("Choose");
		} else {
			alert(data);
		}
		
	}

  componentDidMount() {
    this.animate();
  }

  animate() {
    let progress = 0;
    this.setState({ progress });
    setTimeout(() => {
      setInterval(() => {
        progress += Math.random() / 5;
        if ( progress > 1 ) {
          this.setState({ complete: true })
        }
        this.setState({ progress });
      }, 500);
    }, 1500);
    
  }

  render() {
    return (
      <View style={styles.container}>
      
        <View style = {styles.box} >
        
          <View style={styles.iconContainer}> 
            <Icon name="upload" size={36} color="#000" />
          </View>
          
          <View style={styles.bar} >

          { 
            this.state.complete ?  
            <View style={{flexDirection: 'row'}} > 
              <Icon name="check" size={25} color="#000" />
              <Text> Finishing Up </Text>
            </View>  : 
            <ProgressBar style={{left: '0%'}}
            progress={this.state.progress}
            borderRadius = {0} /> 
          }

          </View>

        </View> 

        <View>
          {this._renderButton('Filter', () => this.setState({ visibleModal: 1 }))}
          <Modal
            isVisible={this.state.visibleModal === 1}
            onBackdropPress={() => this.setState({ visibleModal: false })}
            animationIn={'slideInLeft'}
            animationOut={'slideOutRight'}
          >
            {this._renderModalContent()}

            <View>
              {this._renderButton('Close', () => this.setState({ visibleModal: null }))}
            </View>

          </Modal>
        </View>
        
        
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'space-around',
    alignItems: 'center',
    
  },
  iconContainer: {
    flex: 0.3,
    margin: '2%'
  },
  box: {
    alignSelf: 'stretch', 
    borderWidth: 0.5,
    backgroundColor: '#dee2e8',
    flexDirection: 'row',
    height: '10%',
    alignItems: 'center',
  },
  button: {
    backgroundColor: 'lightblue',
    padding: '4%',
    margin: '8%',
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 4,
    borderColor: 'rgba(0, 0, 0, 0.1)',
  },
  modalContent: {
    backgroundColor: 'white',
    padding: '8%',
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 4,
    borderColor: 'rgba(0, 0, 0, 0.1)',
    flexDirection: 'row' 
  },
  bar: {
    flex: 1.4,
    justifyContent: 'flex-end',
  }
});